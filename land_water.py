#!/usr/bin/env python2
import collections
import functools
import logging
import random
import timeit
import argparse
"""
The map is laid out in a grid and map tiles are connected horizontally and vertically.
There are only two types of tiles: land and water.
Given a map, counts how many land masses are there.
Land-water map is represented as 2-dimensional array, 1 stands for land, 0 stands for water.
"""


def count_lands_using_search(m, use_bfs=True):
    """
    Count land masses of given land-water map.
    When use_bfs is True, uses BFS, otherwise DFS
    """
    height = len(m)
    width = len(m[0])

    def run_search(x, y, use_bfs):
        deq = collections.deque([(x, y)])
        pop_func = deq.popleft if use_bfs else deq.pop  # Works as queue if use_bfs, or as stack otherwise
        while deq:
            x, y = pop_func()
            if x < 0 or y < 0 or x >= height or y >= width or m[x][y] == 0:
                continue
            m[x][y] = 0
            deq.append((x + 1, y))
            deq.append((x - 1, y))
            deq.append((x, y + 1))
            deq.append((x, y - 1))

    result = 0
    for i in xrange(height):
        for j in xrange(width):
            if m[i][j] != 0:
                run_search(i, j, use_bfs=use_bfs)
                result += 1
    return result


def count_lands_using_union_find(m):
    """
    Count land masses using union-find approach. (https://en.wikipedia.org/wiki/Disjoint-set_data_structure)
    Creates DisjointSetNode out of each land cell, then unites each cell with neighbours.
    Each creation of node increases the result value, each unite decreasing.
    """
    result = 0
    height = len(m)
    width = len(m[0])

    for i in xrange(height):
        for j in xrange(width):
            if m[i][j] != 0:
                m[i][j] = DisjointSetNode()
                result += 1

    for i in xrange(height):
        for j in xrange(width):
            if m[i][j] != 0:
                for neighbor_i, neighbor_j in ((i+1, j), (i, j+1)):
                    if neighbor_i < height and neighbor_j < width and m[neighbor_i][neighbor_j] != 0:
                        if union_with_mark(m[i][j], m[neighbor_i][neighbor_j]):
                            result -= 1

    return result


class DisjointSetNode(object):
    """
    Single node of Disjoint set.
    """
    def __init__(self, parent=None):
        self.parent = self if parent is None else parent
        self.mark = 0


def find(node):
    """
    Returns representative of set to which node belongs to.
    """
    nodes = []
    while node.parent != node:
        nodes.append(node)
        node = node.parent
    representative = node
    for node in nodes:
        node.parent = representative
    return representative


def union_with_mark(node1, node2):
    """
    Unites two sets.
    Returns True if nodes were from different set, and False if they already were in same set.
    """
    representative1 = find(node1)
    representative2 = find(node2)
    if representative1 == representative2:
        return False
    if representative1.mark == representative2.mark:
        representative1.parent = representative2
        representative2.mark += 1
    elif representative1.mark > representative2.mark:
        representative2.parent = representative1
    else:
        representative1.parent = representative2
    return True


def generate_land_water_map(height, width, masses):
    """
    Generates land-water map with specified height, width and land masses count, by randomly planting land cells.
    There is no guarantee that it can generate maps for all possible height/width/masses number combinations.
    And if it fails to generate, it will be very slow even for relatively small maps.
    """
    #  TODO: improve existing or add better method for random map generation
    current_masses_count = 0
    result = [[0 for _ in xrange(width)] for _ in xrange(height)]
    logging.info("Generating map: %s x %s, %s land masses", height, width, masses)
    land_cells = 0
    while current_masses_count != masses:
        if land_cells == width * height:
            logging.warning("Filled whole map with land, but hasn't reached desired land masses count, returning None")
            return None
        while True:
            i, j = random.randint(0, height-1), random.randint(0, width-1)
            if result[i][j] == 0:
                break
        result[i][j] = DisjointSetNode()
        land_cells += 1
        current_masses_count += 1
        for ch_i, ch_j in [+1, 0], [-1, 0], [0, +1], [0, -1]:
            neighbor_i, neighbor_j = ch_i + i, ch_j + j
            if height > neighbor_i >= 0 and width > neighbor_j >= 0 and result[neighbor_i][neighbor_j]:
                if union_with_mark(result[i][j], result[neighbor_i][neighbor_j]):
                    current_masses_count -= 1
    for i in xrange(height):
        for j in xrange(width):
            if result[i][j]:
                result[i][j] = 1
    return result


def copy_map(land_water_map):
    """
    Returns a copy of provided land-water map
    """
    height = len(land_water_map)
    width = len(land_water_map[0])
    result = [[0 for _ in xrange(width)] for _ in xrange(height)]
    for i in xrange(height):
        for j in xrange(width):
            if land_water_map[i][j]:
                result[i][j] = 1
    return result


def log_map(land_water_map):
    logging.info('\n'.join(str(level) for level in land_water_map))


def run_tests(measure_perf=False):
    """
    Runs tests of all three versions of solutions on some simple and some big randomly generated land-water maps.
    If measure_perf set to True also measures performance on those maps.
    """
    def test_maps_generator():
        for test_map in [
            (
                    [
                        [1, 0, 1],
                        [0, 0, 1],
                        [1, 1, 0],
                    ],
                    3
            ),
            (
                    [
                        [1, 0, 1, 1, 1],
                        [1, 1, 1, 0, 1],
                    ],
                    1
            ),
            (
                    [
                        [0, 0, 0],
                        [0, 0, 0],
                    ],
                    0
            ),
            (
                    [
                        [0],
                        [1],
                        [0],
                        [1],
                    ],
                    2
            ),
            (
                    [
                        [1, 1, 1],
                        [1, 1, 1],
                        [1, 1, 1],
                    ],
                    1
            ),
        ]:
            yield test_map
        for height, width, masses in [
            (3, 3, 1),
            (5, 10, 6),
            (100, 100, 100),
            (500, 500, 1000),
            (1000, 1000, 5000),
            (5000, 5000, 30000),
        ]:
            yield generate_land_water_map(height, width, masses), masses

    logging.debug("Running tests")
    for land_water_map, answer in test_maps_generator():
        height = len(land_water_map)
        width = len(land_water_map[0])
        if height > 10 or width > 10:
            logging.info("Solving map %s x %s", height, width)
        else:
            logging.info("Solving map:")
            log_map(land_water_map)
        for solver_func, solver_name in [
            (functools.partial(count_lands_using_search, use_bfs=False), "DFS"),
            (functools.partial(count_lands_using_search, use_bfs=True), "BFS"),
            (count_lands_using_union_find, "Union find"),
        ]:
            map_copy = copy_map(land_water_map)
            logging.debug("Solving with %s...", solver_name)
            result = solver_func(map_copy)
            if measure_perf:
                number = 5
                solve_time = timeit.Timer(lambda: solver_func(map_copy)).timeit(number=number) / number
                logging.debug("Took %.4f seconds in average", solve_time)
            assert result == answer, "expected to get {}, got {} with {}".format(answer, result, solver_func)

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format='%(message)s')
    parser = argparse.ArgumentParser()
    parser.add_argument("--perf", help="measure perf", action="store_true")
    args = parser.parse_args()
    run_tests(measure_perf=args.perf)
